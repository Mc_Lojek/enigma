﻿#include <iostream>

using namespace std;

struct Rotor
{
	int* content;
	int* reverse;
	int shift;
	int* triggers;
	int num_triggers;
};

struct Reflector
{
	int* content;
};

struct Machine
{
	int num_letters;
	int num_rotors;
	Rotor* rotors;
	Reflector reflector;
	bool has_2nd_rotated;
};

bool is_trigger(Machine m, int rotor_index)
{
	int flag = false;

	for (int i = 0; i < m.rotors[rotor_index].num_triggers; i++)
	{
		if (m.rotors[rotor_index].triggers[i] == (m.rotors[rotor_index].shift + 1) % m.num_letters)
		{
			flag = true;
			break;
		}
	}
	return flag;
}

int encode(Machine* machine, int input, bool first)
{
	if (!first)
	{
		if (machine->num_rotors > 2 && is_trigger(*machine, 1) && machine->has_2nd_rotated)
		{
			machine->rotors[1].shift++;
			machine->rotors[1].shift %= machine->num_letters;

			machine->rotors[2].shift++;
			machine->rotors[2].shift %= machine->num_letters;
		}
		else if (machine->num_rotors > 1 && is_trigger(*machine, 0) )
		{
			machine->rotors[1].shift++;
			machine->rotors[1].shift %= machine->num_letters;
			machine->has_2nd_rotated = true;
		}
	}

	machine->rotors[0].shift++;
	machine->rotors[0].shift %= machine->num_letters;

	for (int i = 0; i < machine->num_rotors; i++)
	{
		input += machine->rotors[i].content[(input-1+machine->rotors[i].shift)%machine->num_letters];
		input %= machine->num_letters;
		if (input < 1)
			input += machine->num_letters;
	}

	input = machine->reflector.content[input-1];

	for (int i = machine->num_rotors-1; i >= 0; i--)
	{
		input += machine->rotors[i].shift;
		input %= machine->num_letters;
		if (input < 1)
			input += machine->num_letters;

		input -= machine->rotors[i].reverse[(input - 1)];
		input -= machine->rotors[i].shift;
		input %= machine->num_letters;
		if (input < 1)
			input += machine->num_letters;
	}
	return input;
}

int main()
{
	int num_letters = 0;
	int num_rotors = 0;
	int num_triggers = 0;
	int num_reflectors = 0;
	int rotor_temp = 0;
	Rotor* rotors;
	Reflector* reflectors;
	
	cin >> num_letters;
	cin >> num_rotors;

	rotors = new Rotor [num_rotors];
	for (int i = 0; i < num_rotors; i++)
	{
		rotors[i].content = new int[num_letters];
		rotors[i].reverse = new int[num_letters];
		for (int j = 0; j < num_letters; j++)
		{
			cin >> rotor_temp;
			rotors[i].content[j] = rotor_temp - (j + 1);
			rotors[i].reverse[rotor_temp-1] = rotor_temp - (j + 1);
			
		}
		cin >> num_triggers;
		rotors[i].num_triggers = num_triggers;
		rotors[i].triggers = new int [num_triggers];
		for (int j = 0; j < num_triggers; j++)
		{
			cin >> rotors[i].triggers[j];
			rotors[i].triggers[j]--;
		}
	}

	cin >> num_reflectors;
	reflectors = new Reflector [num_reflectors];
	for (int i = 0; i < num_reflectors; i++)
	{
		reflectors[i].content = new int[num_letters];
		for (int j = 0; j < num_letters; j++)
		{
			cin >> reflectors[i].content[j];
		}
	}
	//end of setup loading
	

	int num_words;
	int rotor_index;
	int rotor_shift;
	int reflector_index;
	int buffor;
	Machine machine;
	machine.num_letters = num_letters;
	machine.has_2nd_rotated = false;

	cin >> num_words;

	for (int i = 0; i < num_words; i++)
	{
		cin >> machine.num_rotors;
		machine.rotors = new Rotor[machine.num_rotors];
		for (int j = 0; j < machine.num_rotors; j++)
		{
			cin >> rotor_index;
			cin >> rotor_shift;

			machine.rotors[j] = rotors[rotor_index];
			machine.rotors[j].shift = rotor_shift-1;
		}

		cin >> reflector_index;
		machine.reflector = reflectors[reflector_index];

		bool first = true;
		machine.has_2nd_rotated = false;
		do
		{
			cin >> buffor;
			if(buffor!=0)
				cout<<encode(&machine, buffor, first);
			cout << " ";
			first = false;
		}
		while (buffor != 0);
		cout << endl;
		
	}

	delete[] rotors;
	delete[] reflectors;
}
